import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/notification/components/defaultAppBar.dart';
import 'package:flutter_auth/component_update/notification/components/defaultBackButton.dart';
import 'package:flutter_auth/component_update/products/productsDetail.dart';
import 'package:flutter_auth/service/product.dart';
import 'package:flutter_auth/widget/categoryItems.dart';
import 'package:flutter_auth/widget/categoryView.dart';
import 'package:flutter_auth/widget/recommendedItems.dart';
import 'package:flutter_auth/widget/recommendedView.dart';

class Products extends StatefulWidget {
  bool isRecommended;
  String title;
  String id;
  Products(this.isRecommended, this.title, this.id);
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  Product_Service _product_service = new Product_Service();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: DefaultAppBar(
        title: widget.title.toString(),
        child: DefaultBackButton(),
      ),
      body: widget.isRecommended
          ? FutureBuilder(
          future: Product_Service.fetchProductbyType(widget.id),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return RecommendedView(
                direction: Axis.vertical,
                heights: MediaQuery.of(context).size.height,
                widths: MediaQuery.of(context).size.width,
                top: 0.0,
                bottom: 0.0,
                left: 0.0,
                right: 0.0,
                column: 1,
                ratio: 4,
                items: snapshot.data.length,
                itemBuilder: (context, index) {
                  print(snapshot.data[index].quantity_sold.toString());
                  return GestureDetector(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ProductDetails(product:snapshot.data[index]),
                      ),
                    ),
                    child: RecommendedItems(
                      height: 165.0,
                      radius: 8.0,
                      top: 4.0,
                      bottom: 4.0,
                      left: 6.0,
                      right: 6.0,
                      image: snapshot.data[index].img,
                      title: snapshot.data[index].name,
                      price: snapshot.data[index].price_origin,
                      rating: snapshot.data[index].rating,
                      sale:  snapshot.data[index].quantity_sold,
                    ),
                  );
                },
              );
            } else {
              return Center(child: Text('empty'));
            }
          })

          :
      FutureBuilder(
          future: Product_Service.fetchProduct_best_sell(),
          builder:(context,snapshot){
            if(snapshot.hasData){
              return  Categoryview(
                direction: Axis.vertical,
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: kWhiteColor,
                column: 1,
                ratio: 1.0,
                items: snapshot.data.length,
                itemBuilder: (context, index) {
                  return CategoryItems(
                    height: MediaQuery.of(context).size.height,
                    width: 400.0,
                    paddingVertical: 4.0,
                    paddingHorizontal: 16.0,
                    radius: 0.0,
                    image: snapshot.data[index].img,
                    amount: '10%',
                    lblColor: kPrimaryColor,
                    align: Alignment.topRight,
                  );
                },
              );
            }
            else{
              return Center(child: Text('321312'));
            }
          }
      ),
    );
  }
}
