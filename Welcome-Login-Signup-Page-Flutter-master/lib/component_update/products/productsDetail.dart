import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/notification/components/defaultAppBar.dart';
import 'package:flutter_auth/component_update/notification/components/defaultBackButton.dart';
import 'package:flutter_auth/component_update/page/components/bottomNavBar.dart';
import 'package:flutter_auth/event/product/Product.dart';
import 'package:flutter_auth/service/product.dart';
import 'package:flutter_auth/widget/stickyBel.dart';

class ProductDetails extends StatefulWidget {
  ProductEvent product;
  ProductDetails({Key key, this.product}) : super(key: key);
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  int currentIndex = 0;
  bool checked_size = false;
  dynamic _key = new GlobalKey<ScaffoldState>();
  PageController pageController = PageController(initialPage: 0);
  bool isMore = false;
  // CartBloc _cartBloc = new CartBloc();
  String _size;
  // List<String> productSize = [
  //   'S',
  //   'M',
  //   'L',
  //   'XL',
  // ];
  //
  // List<Color> productColors = [
  //   kPrimaryColor,
  //   kDarkColor,
  //   Colors.orange,
  //   Colors.green,
  // ];

  // List<String> detailImages = [
  //   'image/backgr.jpg',
  //   'image/backgr.jpg',
  //   'image/backgr.jpg',
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      backgroundColor: kWhiteColor,
      appBar: DefaultAppBar(
        title: 'Product Details',
        child: DefaultBackButton(),
        action: [
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => BottomNavBar(2),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Material(
        elevation: kLess,
        color: kWhiteColor,
        child: Row(
          children: [
            // Container(
            //   margin: EdgeInsets.only(right: 8.0, left: 8.0),
            //   decoration: BoxDecoration(
            //     border: Border.all(color: kPrimaryColor, width: 2.0),
            //   ),
            //   child: IconButton(
            //     icon: Icon(Icons.add_shopping_cart, color: kPrimaryColor),
            //     onPressed: () {
            //       if (_size ==null) {
            //         _key.currentState.showSnackBar(SnackBar(
            //             backgroundColor: Colors.lightBlue,
            //             content: Text("choose size before add to cart")));
            //       } else {
            //         onAddToCart();
            //         _key.currentState
            //             .showSnackBar(SnackBar(
            //             backgroundColor: Colors.lightBlue,
            //             content: Text("Add success")));
            //       }
            //     },
            //   ),
            // ),
            // Container(
            //   margin: EdgeInsets.only(right: 8.0),
            //   decoration: BoxDecoration(
            //     border: Border.all(color: kPrimaryColor, width: 2.0),
            //   ),
            //   child: IconButton(
            //     icon: Icon(Icons.chat, color: kPrimaryColor),
            //     onPressed: () => Navigator.of(context).push(
            //       MaterialPageRoute(
            //         builder: (context) => CommentList(
            //           product: widget.product,
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            // Expanded(
            //   child: Container(
            //     height: 52.0,
            //     decoration: BoxDecoration(
            //       border: Border.all(color: kPrimaryColor, width: 2.0),
            //     ),
            //     child: FlatButton(
            //         padding: EdgeInsets.symmetric(vertical: kLessPadding),
            //         color: kPrimaryColor,
            //         textColor: kWhiteColor,
            //         child: Text('Pay', style: TextStyle(fontSize: 18.0)),
            //         onPressed: () {
            //           onAddToCart();
            //           Navigator.of(context).push(
            //             MaterialPageRoute(
            //               builder: (context) => DeliveryAddress(),
            //             ),
            //           );
            //         }),
            //   ),
            // ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  height: 250.0,
                  child: PageView.builder(
                    controller: pageController,
                    onPageChanged: (value) {
                      setState(() {
                        currentIndex = value;
                      });
                    },
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return Image.network(
                        widget.product.img,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                      );
                    },
                  ),
                ),
                Positioned(
                  bottom: 16.0,
                  left: 0.0,
                  right: 0.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      1,
                          (index) => AnimatedContainer(
                        duration: Duration(milliseconds: 400),
                        height: 8.0,
                        width: currentIndex == index ? 24.0 : 8.0,
                        margin: EdgeInsets.only(right: 4.0),
                        decoration: BoxDecoration(
                          color: currentIndex == index
                              ? kPrimaryColor
                              : kWhiteColor,
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            StickyLabel(text: widget.product.name),
            Padding(
              padding: EdgeInsets.only(
                left: kDefaultPadding,
              ),
              child: Text(
                '\$ ${widget.product.price_origin}',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: kDefaultPadding,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 12.0,
                        vertical: 4.0,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(22.0),
                      ),
                      child: Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: widget.product.rating,
                              style: TextStyle(
                                color: kWhiteColor,
                                fontSize: 16.0,
                              ),
                            ),
                            WidgetSpan(
                              alignment: PlaceholderAlignment.top,
                              child: Icon(
                                Icons.star,
                                size: 18.0,
                                color: Colors.orange,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: Text(
                      '${widget.product.quantity_sold} Sale',
                      style: TextStyle(
                        color: kDarkColor.withOpacity(0.4),
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: kLessPadding),
            kSmallDivider,
            Padding(
              padding: EdgeInsets.only(
                left: kDefaultPadding,
              ),
              child: Text(
                'Size',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            Container(
              height: 44.0,
              margin: EdgeInsets.symmetric(
                  horizontal: kDefaultPadding, vertical: kLessPadding),
              child: FutureBuilder(
                  future: Product_Service.GetSizeInProduct(widget.product.id),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          print(snapshot.data[index]);
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                _size = snapshot.data[index].size;
                              });
                              //  print(_size);
                            },
                            child: Container(
                              //  color: Colors.pink,
                              height: 44.0,
                              width: 44.0,
                              margin: EdgeInsets.only(right: 8.0),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: _size == snapshot.data[index].size
                                    ? Colors.pink.withOpacity(0.5)
                                    : Colors.white,
                                border: Border.all(
                                  width: 3.0,
                                  color: kDarkColor.withOpacity(0.4),
                                ),
                                borderRadius: BorderRadius.circular(44.0),
                              ),
                              child: Text(
                                snapshot.data[index].size,
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: kDarkColor.withOpacity(0.4),
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    } else {
                      return Container(
                        child: CircularProgressIndicator(),
                      );
                    }
                  }),
            ),
            kSmallDivider,
            Padding(
              padding: EdgeInsets.only(
                left: kDefaultPadding,
              ),
              child: Text(
                'Color',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            Container(
              height: 44.0,
              margin: EdgeInsets.symmetric(
                  horizontal: kDefaultPadding, vertical: kLessPadding),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 1,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () =>
                        print('Selected Color:${widget.product.color}'),
                    child: Container(
                      height: 44.0,
                      width: 44.0,
                      margin: EdgeInsets.only(right: 8.0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        //color: productColors[index],
                        //   color: Color(int.parse('0x'+widget.product.color)),
                        color:Color(0x6495ed),
                        border: Border.all(
                          width: 3.0,
                          color: kDarkColor.withOpacity(0.4),
                        ),
                        borderRadius: BorderRadius.circular(44.0),
                      ),
                    ),
                  );
                },
              ),
            ),
            kSmallDivider,
            Padding(
              padding: EdgeInsets.only(
                left: kDefaultPadding,
              ),
              child: Text(
                'Description',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: kDefaultPadding,
                  right: kDefaultPadding,
                  top: kLessPadding),
              child: isMore
                  ? Text(widget.product.description, style: kSubTextStyle)
                  : Text(
                widget.product.description,
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
                style: kSubTextStyle,
              ),
            ),
            GestureDetector(
              onTap: () => setState(() {
                isMore = !isMore;
              }),
              child: Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: kDefaultPadding),
                  child: Text(
                    isMore ? 'View Less' : 'View More',
                    style: TextStyle(color: kPrimaryColor, fontSize: 20.0),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: kLessPadding,
            )
          ],
        ),
      ),
    );
  }

//   dynamic onAddToCart() {
//
//     _cartBloc.event.add(CartEvent(
//         widget.product.id,
//         _size,
//         widget.product.color,
//         "1",
//         widget.product.price_origin,
//         widget.product.name,
//         widget.product.img));
//   }
}
