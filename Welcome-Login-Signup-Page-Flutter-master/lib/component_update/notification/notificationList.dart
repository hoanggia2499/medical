
import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';

import 'components/defaultAppBar.dart';
import 'components/defaultBackButton.dart';
import 'components/notificationTitles.dart';
import 'notificationPage.dart';

class NotidicationList extends StatefulWidget {
  NotidicationList({Key key}) : super(key: key);

  @override
  _NotidicationListState createState() => _NotidicationListState();
}

class _NotidicationListState extends State<NotidicationList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: DefaultAppBar(
        title: 'Notifications',
        child: DefaultBackButton(),
      ),
      body: ListView.separated(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.zero,
          itemCount: 12,
          itemBuilder: (context, index) {
            return NotificationTitles(
              title: 'E-Commerce',
              subtitle: 'Thanks for download E-Commerce app.',
              enable: true,
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => NotificationPage())),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          }),
    );
  }
}
