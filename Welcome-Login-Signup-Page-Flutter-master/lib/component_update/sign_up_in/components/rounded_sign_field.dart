import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';

import 'defaultTextFiels.dart';

class CenterTextField extends StatelessWidget {
  final TextEditingController fullname;
  final TextEditingController email;
  final TextEditingController password;
  final TextEditingController confirmPassword;
  final TextEditingController phone;
  final TextEditingController address;
  final Function test;
  final String data, hintText;
  const CenterTextField({
    Key key,
    this.data,
    this.hintText,
    this.test,
    this.fullname,
    this.email,
    this.phone,
    this.address,
    this.password,
    this.confirmPassword,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // bool _showPass = true;

    return Expanded(
      flex: 0,
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        DefaultTextField(
          test: test,
          textController: fullname,
          hintText: 'FullName',
          icon: Icons.person,
          keyboardType: TextInputType.text,
          obscureText: false,
          readOnly: false,
        ),
        SizedBox(
          height: kDefaultPadding,
        ),
        DefaultTextField(
          test: test,
          textController: email,
          hintText: 'Email',
          icon: Icons.email,
          keyboardType: TextInputType.text,
          obscureText: false,
          readOnly: false,
        ),
        SizedBox(
          height: kDefaultPadding,
        ),
        DefaultTextField(
          test: test,
          //  test: test(data, hintText),
          textController: password,
          hintText: 'Password',
          icon: Icons.lock,
          keyboardType: TextInputType.visiblePassword,
          obscureText: true,
          readOnly: false,
          // suffixIcon: IconButton(
          //   icon: Icon(
          //     // Based on passwordVisible state choose the icon
          //     _showPass ? Icons.visibility : Icons.visibility_off,
          //     color: kPrimaryColor,
          //   ),
          // ),
        ),
        SizedBox(
          height: kDefaultPadding,
        ),
        DefaultTextField(
          test: test,
          //  test: test(data, hintText),
          textController: confirmPassword,
          hintText: 'Confirm Password',
          icon: Icons.lock,
          keyboardType: TextInputType.text,
          obscureText: true,
          readOnly: false,
        ),
        SizedBox(
          height: kDefaultPadding,
        ),
        DefaultTextField(
          test: test,
          readOnly: false,
          //  test: test(data, hintText),
          textController: phone,
          hintText: 'Phone',
          icon: Icons.phone,
          keyboardType: TextInputType.text,
          obscureText: false,
        ),
        SizedBox(
          height: kDefaultPadding,
        ),
        DefaultTextField(
          test: test,
          //  test: test(data, hintText),
          textController: address,
          hintText: 'Address',
          icon: Icons.directions,
          keyboardType: TextInputType.text,
          obscureText: false,
          readOnly: false,
        ),
      ]),
    );
  }
}
