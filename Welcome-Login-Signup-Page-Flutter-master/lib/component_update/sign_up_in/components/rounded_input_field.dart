import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/text_field_container.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  final TextEditingController textControler;
  final String errorHint;
  final TextAlignVertical;
  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
    this.textControler,
    this.errorHint,
    this.TextAlignVertical
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(

        controller: textControler,
        onChanged: onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: kPrimaryColor,
          ),
          hintText: hintText,
          errorText: errorHint==null?"":errorHint,
            // errorHint.hasError ? errorHint.error.toString() : null,
            // labelStyle: TextStyle(
            //     fontSize: 15, color: Colors.grey[300])),

          border: InputBorder.none,
        ),
      ),
    );
  }
}
