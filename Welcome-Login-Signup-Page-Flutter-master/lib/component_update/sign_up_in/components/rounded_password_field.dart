import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/text_field_container.dart';

class RoundedPasswordField extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final TextEditingController textController;
  final String errorHint;
  final String hintText;

  const RoundedPasswordField(
      {Key key,
      this.onChanged,
      this.textController,
      this.errorHint,
      this.hintText})
      : super(key: key);

  @override
  _RoundedPasswordFieldState createState() => _RoundedPasswordFieldState();
}

class _RoundedPasswordFieldState extends State<RoundedPasswordField> {
  bool _showPass = true;

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        controller: widget.textController,
        obscureText: _showPass,
        onChanged: widget.onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          icon: Icon(
            Icons.lock,
            color: kPrimaryColor,
          ),
          hintText: 'Passworld',
          errorText: widget.errorHint == null ? "" : widget.errorHint,
          border: InputBorder.none,
          suffixIcon: IconButton(
            icon: Icon(
              // Based on passwordVisible state choose the icon
              _showPass ? Icons.visibility : Icons.visibility_off,
              color: kPrimaryColor,
            ),
            onPressed: () {
              // Update the state i.e. toogle the state of passwordVisible variable
              setState(() {
                _showPass = !_showPass;
              });
            },
          ),
        ),
      ),
    );
  }
}
