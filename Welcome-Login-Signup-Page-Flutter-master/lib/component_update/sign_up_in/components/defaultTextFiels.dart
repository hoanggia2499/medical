
import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/message/message.dart';
class DefaultTextField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final keyboardType;
  final obscureText;
  final TextEditingController textController;
  final Function addError;
  final Function removeError;
  final Function test ;
  final bool readOnly;
  final suffixIcon;
  const DefaultTextField({
    Key key,
    this.textController,
    this.obscureText,
    this.readOnly,
    this.addError,
    this.removeError,
    this.hintText,
    this.icon,
    this.keyboardType,
    this.test,
    this.suffixIcon
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: kDefaultPadding),
      padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular((kShape))),
        color: kPrimaryLightColor,
      ),
      child: TextField(


        onChanged: (text) {
          if(test!=null)
          {
            test(hintText,text);
          }
          if (text.isNotEmpty) {

            addError(error: mess_NullError + hintText.toString());
          } else if (text.length >= 8) {
            removeError(error: mess_ShortError + hintText.toString());
          }
          return null;


        //  print('First text field: $text $hintText');
        },
        controller: textController,
        cursorColor: kPrimaryColor,
        textInputAction: TextInputAction.next,
        keyboardType: keyboardType,
        obscureText: obscureText,
        readOnly: readOnly,
        decoration: InputDecoration(
          border: InputBorder.none,
          icon: Icon(icon),
          hintText: hintText,
          // suffixIcon: suffixIcon(),
        ),
      ),
    );
  }
}
