import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/sign_up_in/Screens/Login/login_screen.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/already_have_an_account_acheck.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/rounded_button.dart';

class BottomWidgets extends StatelessWidget {
  final Function abc;
  const BottomWidgets({
    Key key,
    this.abc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        RoundedButton(
          text: "SIGN UP",
          press: () {
            abc(context);
          },
        ),
        AlreadyHaveAnAccountCheck(
          login: false,
          press: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return LoginScreen();
                },
              ),
            );
          },
        ),
      ]),
    );
  }

  // dynamic onSignInClicked(BuildContext context) {
  //   print('onSignInClicked');
  //
  //   signUpBloc.event.add(SignUpEvent(
  //       fullname.text,
  //       email.text,
  //       password.text,
  //       confirmPassword.text,
  //       phone.text,
  //       address.text,
  //       context));
  //
  // }
}
