import 'package:flutter/material.dart';
import 'package:flutter_auth/bloc/signUp_bloc.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/sign_up_in/Screens/Signup/components/BottomWidgets.dart';
import 'package:flutter_auth/component_update/sign_up_in/Screens/Signup/components/background.dart';
import 'package:flutter_auth/component_update/sign_up_in/Screens/Signup/components/or_divider.dart';
import 'package:flutter_auth/component_update/sign_up_in/Screens/Signup/components/social_icon.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/rounded_sign_field.dart';
import 'package:flutter_auth/event/user/SignUp.dart';
import 'package:flutter_auth/intro/components/emptyAppBar.dart';

class SignUp extends StatefulWidget {
  SignUp({Key key}) : super(key: key);
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String fullname;
  String email;
  String password;
  String confirmPassword;
  String phone;
  String address;
  String data, hintText;
  SignUpBloc _signUpBloc = new SignUpBloc();
  Function abc1(String hintText, String data) {
    print('$hintText : $data');
    if (hintText == 'FullName') {
      fullname = data;
    }
    if (hintText == 'Email') {
      email = data;
    }
    if (hintText == 'Password') {
      password = data;
    }
    if (hintText == 'ConfirmPassword') {
      confirmPassword = data;
    }
    if (hintText == 'Phone') {
      phone = data;
    }
    if (hintText == 'Address') {
      address = data;
    }
  }

  Function call(BuildContext context) {
    print('onSignInClicked');
    _signUpBloc.event.add(SignUpEvent(
        fullname, email, password, confirmPassword, phone, address, context));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      // appBar: AppBar(
      //   title: Text('SIGN UP'),
      // ),
      child: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(children: [
            // TopLogo(),
            SizedBox(height: size.height * 0.03),
            CenterTextField(
              test: abc1,
            ),
            BottomWidgets(
              abc: call,
            ),
            SizedBox(height: size.height * 0.02),
            OrDivider(),
            SizedBox(height: size.height * 0.02),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  iconSrc: "assets/icons/facebook.svg",
                  press: () {},
                ),
                SocalIcon(
                  iconSrc: "assets/icons/twitter.svg",
                  press: () {},
                ),
                SocalIcon(
                  iconSrc: "assets/icons/google-plus.svg",
                  press: () {},
                ),
                // SizedBox(height: size.height * 0.03),
              ],
            )
          ]),
        ),
      ),
    );
  }
}

// import 'package:flutter/material.dart';
// import 'package:flutter_auth/component_update/sign_up_in/Screens/Login/login_screen.dart';
// import 'package:flutter_auth/component_update/sign_up_in/Screens/Signup/components/background.dart';
// import 'package:flutter_auth/component_update/sign_up_in/Screens/Signup/components/or_divider.dart';
// import 'package:flutter_auth/component_update/sign_up_in/Screens/Signup/components/social_icon.dart';
// import 'package:flutter_auth/component_update/sign_up_in/components/already_have_an_account_acheck.dart';
// import 'package:flutter_auth/component_update/sign_up_in/components/rounded_button.dart';
// import 'package:flutter_auth/component_update/sign_up_in/components/rounded_input_field.dart';
// import 'package:flutter_auth/component_update/sign_up_in/components/rounded_password_field.dart';
// import 'package:flutter_auth/component_update/sign_up_in/components/text_field_container.dart';
// import 'package:flutter_svg/svg.dart';
//
// class Body extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Background(
//       child: SingleChildScrollView(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             SizedBox(height: size.height * 0.03),
//             Text(
//               "SIGNUP",
//               style: TextStyle(fontWeight: FontWeight.bold),
//             ),
//             // SizedBox(height: size.height * 0.03),
//             RoundedInputField(
//               hintText: "Full name",
//               onChanged: (value) {},
//             ),
//             RoundedInputField(
//               hintText: "Your Email",
//               onChanged: (value) {},
//             ),
//             RoundedPasswordField(
//               onChanged: (value) {},
//             ),
//             RoundedPasswordField(
//               hintText: 'Confirm Passworld',
//               onChanged: (value) {},
//             ),
//             RoundedInputField(
//               hintText: "Phone",
//               onChanged: (value) {},
//             ),
//             RoundedInputField(
//               hintText: "Address",
//               onChanged: (value) {},
//             ),
//             RoundedButton(
//               text: "SIGNUP",
//               press: () {},
//             ),
//
//             AlreadyHaveAnAccountCheck(
//               login: false,
//               press: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) {
//                       return LoginScreen();
//                     },
//                   ),
//                 );
//               },
//             ),
//             OrDivider(),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 SocalIcon(
//                   iconSrc: "assets/icons/facebook.svg",
//                   press: () {},
//                 ),
//                 SocalIcon(
//                   iconSrc: "assets/icons/twitter.svg",
//                   press: () {},
//                 ),
//                 SocalIcon(
//                   iconSrc: "assets/icons/google-plus.svg",
//                   press: () {},
//                 ),
//                 SizedBox(height: size.height * 0.03),
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
