import 'package:flutter/material.dart';
import 'package:flutter_auth/bloc/login_bloc/auth_bloc.dart';
import 'package:flutter_auth/component_update/sign_up_in/Screens/Login/components/background.dart';
import 'package:flutter_auth/component_update/sign_up_in/Screens/Signup/signup_screen.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/already_have_an_account_acheck.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/rounded_button.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/rounded_input_field.dart';
import 'package:flutter_auth/component_update/sign_up_in/components/rounded_password_field.dart';
import 'package:flutter_auth/event/user/SignIn.dart';
import 'package:flutter_svg/svg.dart';

class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  AuthBloc bloc = new AuthBloc();

  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "LOGIN",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            SvgPicture.asset(
              "assets/icons/login.svg",
              height: size.height * 0.35,
            ),
            SizedBox(height: size.height * 0.03),
            StreamBuilder(
              stream: bloc.userNameStream,
              builder: (context, snapshot) => RoundedInputField(
                  textControler: _usernameController,
                  errorHint:
                      snapshot.hasError ? snapshot.error.toString() : null,
                  // child:
                  hintText: "Your Email",
                  onChanged: (value) {}),
            ),
            StreamBuilder(
              stream: bloc.passWordStream,
              builder: (context, snapshot) => RoundedPasswordField(
                textController: _passwordController,
                // child:
                //   hintText: "Your Pass",
                onChanged: (value) {},
              ),
            ),
            // RoundedPasswordField(
            //   onChanged: (value) {},
            // ),
            RoundedButton(
              text: "LOGIN",
              press: () {
                _OnSignInClicked(context);
              },
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  void _OnSignInClicked(BuildContext context) {
    setState(() {
      if (bloc.isValidInfo(
          _usernameController.text, _passwordController.text)) {
        bloc.event
            .add(SignInEvent(_usernameController.text, _passwordController.text, context));
       // Navigator.push(context, MaterialPageRoute(builder: gotohome));
      }
    });
  }

  // Widget gotohome(BuildContext context) {
  //   return dashboard();
  // }
}
