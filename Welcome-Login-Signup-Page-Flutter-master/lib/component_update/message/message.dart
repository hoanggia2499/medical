//
// import 'package:flutter/material.dart';
// import 'package:flutter_auth/component_update/constant/constant.dart';
// import 'package:shop_flutter/component_update/about/components/emptySection.dart';
// import 'package:shop_flutter/component_update/constant/constant.dart';
// import 'package:shop_flutter/component_update/notification/components/defaultAppBar.dart';
// import 'package:shop_flutter/component_update/notification/components/defaultBackButton.dart';
//
// class Message extends StatefulWidget {
//   Message({Key key}) : super(key: key);
//
//   @override
//   _MessageState createState() => _MessageState();
// }
//
// class _MessageState extends State<Message> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: kWhiteColor,
//       appBar: DefaultAppBar(
//         title: 'Message',
//         child: DefaultBackButton(),
//       ),
//       body: EmptySection(
//         emptyImg: chatBubble,
//         emptyMsg: 'No message yet',
//       ),
//     );
//   }
// }
