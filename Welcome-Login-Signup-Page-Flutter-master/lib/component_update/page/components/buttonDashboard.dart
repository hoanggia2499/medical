import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/fadedAnimation.dart';

class buttonDashboard extends StatelessWidget {
  final Function press;
  final String text;
  final String picture;

  const buttonDashboard({
    Key key,
    this.press,
    this.text,
    this.picture
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: press,
        child: FadeAnimation(
          0,
          Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.symmetric(vertical: 10),
            width: size.width * 0.35,
            height: size.height * 0.15,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: kPrimaryColor,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(picture),
              ),
              border: Border.all(
                width: 2,
                color: kPrimaryColor,
              )
            ),
            child: Text(
              text,
              style: TextStyle(
                fontSize: 15,
                color: kPrimaryLightColor,
                fontWeight: FontWeight.bold,
              ),
            ),

          ),
        ));
  }
}
