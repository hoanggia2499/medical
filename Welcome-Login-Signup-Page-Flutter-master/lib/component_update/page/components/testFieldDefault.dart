import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';

class TextFieldDefault extends StatelessWidget {
  final Widget child;
  const TextFieldDefault({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      // margin: EdgeInsets.symmetric(vertical: 0),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.45,
      height: size.height * 0.5,
      decoration: BoxDecoration(
        color: Colors.transparent,
        // borderRadius: BorderRadius.circular(29),
        border: Border.all(color: kPrimaryColor)
      ),

      child: child,
    );
  }
}
