
import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/page/barcode/barcode.dart';
import 'package:flutter_auth/component_update/page/cart/cart.dart';
import 'package:flutter_auth/component_update/page/dashboard/dashboard.dart';
import 'package:flutter_auth/component_update/page/management/account.dart';
import 'package:flutter_auth/component_update/page/revenue/category.dart';

class BottomNavBar extends StatefulWidget {
  int currentIndex;
  BottomNavBar(this.currentIndex);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  // int _currentIndex = 0;



  final List<Widget> _children = [
    dashboard(),
    revenue(),
    barcode(),
    cart(),
    management(),

  ];

  void onTapTapped(int index) {
    setState(() {
      widget.currentIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      body: _children[widget.currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTapTapped,
        currentIndex: widget.currentIndex,
        elevation: 0,
        selectedItemColor: kPrimaryColor,
        unselectedItemColor: kLightColor,
        backgroundColor: kWhiteColor,
        // showSelectedLabels: true,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.price_change_sharp),
            title: Text('Revenue'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.qr_code_2_rounded),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Cart'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Management'),
          ),
        ],
      ),
    );
  }
}
