import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/page/detailpages/1.OTC/components/body.dart';

import 'package:flutter_svg/svg.dart';

class function_OTC extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: Body(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      // leading: IconButton(
      //   icon: SvgPicture.asset("assets/icons/back.svg"),
      //   onPressed: () {},
      // ),
      actions: <Widget>[
        // IconButton(
        //   icon: SvgPicture.asset(
        //     "assets/icons/search.svg",
        //     // By default our  icon color is white
        //     color: kTextColor,
        //   ),
        //   onPressed: () {},
        // ),
        // IconButton(
        //   icon: SvgPicture.asset(
        //     "assets/icons/cart.svg",
        //     // By default our  icon color is white
        //     color: kTextColor,
        //   ),
        //   onPressed: () {},
        // ),
        // SizedBox(width: kDefaultPaddin / 2)
      ],
    );
  }
}


class MedicineTray {
  final String image, title, description;
  final int unit, size, id;
  final Color color;
  MedicineTray({
    this.id,
    this.image,
    this.title,
    this.unit,
    this.description,
    this.size,
    this.color,
  });
}

List<MedicineTray> MTray = [
  MedicineTray(
    id: 1,
    title: "MedicineTray's Name",
    unit: 234,
    size: 12,
    description: dummyText,
    image: "assets/images/add.jpg",
    color: Color(0xFF3D82AE),
  ),
  MedicineTray(
      id: 2,
      title: "MedicineTray's Name",
      unit: 234,
      size: 8,
      description: dummyText,
      image: "assets/images/add.jpg",
      color: Color(0xFFD3A984)),
  MedicineTray(
      id: 3,
      title: "MedicineTray's Name",
      unit: 234,
      size: 10,
      description: dummyText,
      image: "assets/images/add.jpg",
      color: Color(0xFF989493)),
  MedicineTray(
      id: 4,
      title: "MedicineTray's Name",
      unit: 234,
      size: 11,
      description: dummyText,
      image: "assets/images/add.jpg",
      color: Color(0xFFE6B398)),
  MedicineTray(
      id: 5,
      title: "MedicineTray's Name",
      unit: 234,
      size: 12,
      description: dummyText,
      image: "assets/images/add.jpg",
      color: Color(0xFFFB7883)),
  MedicineTray(
    id: 6,
    title: "MedicineTray's Name",
    unit: 234,
    size: 12,
    description: dummyText,
    image: "assets/images/add.jpg",
    color: Color(0xFFAEAEAE),
  ),
  MedicineTray(
      id: 7,
      title: "MedicineTray's Name",
      unit: 234,
      size: 12,
      description: dummyText,
      image: "assets/images/add.jpg",
      color: Color(0xFFFB7811)),
  MedicineTray(
    id: 8,
    title: "MedicineTray's Name",
    unit: 234,
    size: 12,
    description: dummyText,
    image: "assets/images/add.jpg",
    color: Color(0xFFAEAEFF),
  ),
];

String dummyText =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. When an unknown printer took a galley.";
