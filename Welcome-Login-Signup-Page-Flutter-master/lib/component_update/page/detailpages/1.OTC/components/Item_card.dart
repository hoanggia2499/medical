import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/page/detailpages/1.OTC/OTC.dart';


class ItemCard extends StatelessWidget {
  final MedicineTray Tray;
  final Function press;
  const ItemCard({
    Key key,
    this.Tray,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.all(10),
              // For  demo we use fixed height  and width
              // Now we dont need them
              // height: 180,
              // width: 160,
              decoration: BoxDecoration(
                  color: Tray.color,
                  borderRadius: BorderRadius.circular(16),
                  border: Border.all(color: kPrimaryColor)
              ),
              child: Hero(
                tag: "${Tray.id}",
                child: Image.asset(Tray.image),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Text(
              // Trays is out demo list
              Tray.title,
              style: TextStyle(color: kPrimaryLightColor, fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            "${Tray.unit} viên",
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
