import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/page/components/testFieldDefault.dart';
import 'package:flutter_auth/component_update/page/detailpages/1.OTC/OTC.dart';
import 'package:flutter_auth/component_update/page/detailpages/1.OTC/components/Item_card.dart';
import 'package:flutter_auth/component_update/products/products.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: kPrimaryLightColor,
      appBar: AppBar(
        title: Text(
          "Quầy thuốc",
          style: TextStyle(),
        ),
        elevation: 0,
        backgroundColor: kPrimaryLightColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: 45,
                child: TextFieldDefault(
                  child: TextField(
                    // controller: textControler,
                    // onChanged: onChanged,
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.person,
                        color: kPrimaryColor,
                      ),
                      hintText: "Nhập tên bệnh nhân",
                      // errorText: errorHint==null?"":errorHint,
                      // errorHint.hasError ? errorHint.error.toString() : null,
                      // labelStyle: TextStyle(
                      //     fontSize: 15, color: Colors.grey[300])),

                      border: InputBorder.none,
                    ),
                  ),
                ),

                // decoration: BoxDecoration(
                //     color: Theme.of(context).primaryColor,
                //   borderRadius: BorderRadius.only(
                //     bottomRight: Radius.circular(50),
                //     bottomLeft: Radius.circular(50)
                //   ))
              ),
              Container(
                height: 45,
                child: TextFieldDefault(
                  child: TextField(
                    // controller: textControler,
                    // onChanged: onChanged,
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.person,
                        color: kPrimaryColor,
                      ),
                      hintText: "Tuổi",
                      // errorText: errorHint==null?"":errorHint,
                      // errorHint.hasError ? errorHint.error.toString() : null,
                      // labelStyle: TextStyle(
                      //     fontSize: 15, color: Colors.grey[300])),

                      border: InputBorder.none,
                    ),
                  ),
                ),

                // decoration: BoxDecoration(
                //     color: Theme.of(context).primaryColor,
                //   borderRadius: BorderRadius.only(
                //     bottomRight: Radius.circular(50),
                //     bottomLeft: Radius.circular(50)
                //   ))
              ),
            ],
          ),
          SizedBox(
            height: size.height * 0.01,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: 45,
                child: TextFieldDefault(
                  child: TextField(
                    // controller: textControler,
                    // onChanged: onChanged,
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.person,
                        color: kPrimaryColor,
                      ),
                      hintText: "Giới tính",
                      // errorText: errorHint==null?"":errorHint,
                      // errorHint.hasError ? errorHint.error.toString() : null,
                      // labelStyle: TextStyle(
                      //     fontSize: 15, color: Colors.grey[300])),

                      border: InputBorder.none,
                    ),
                  ),
                ),

                // decoration: BoxDecoration(
                //     color: Theme.of(context).primaryColor,
                //   borderRadius: BorderRadius.only(
                //     bottomRight: Radius.circular(50),
                //     bottomLeft: Radius.circular(50)
                //   ))
              ),
              Container(
                height: 45,
                child: TextFieldDefault(
                  child: TextField(
                    // controller: textControler,
                    // onChanged: onChanged,
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.person,
                        color: kPrimaryColor,
                      ),
                      hintText: "Chuẩn đoán",
                      // errorText: errorHint==null?"":errorHint,
                      // errorHint.hasError ? errorHint.error.toString() : null,
                      // labelStyle: TextStyle(
                      //     fontSize: 15, color: Colors.grey[300])),

                      border: InputBorder.none,
                    ),
                  ),
                ),

                // decoration: BoxDecoration(
                //     color: Theme.of(context).primaryColor,
                //   borderRadius: BorderRadius.only(
                //     bottomRight: Radius.circular(50),
                //     bottomLeft: Radius.circular(50)
                //   ))
              ),
            ],
          ),
          SizedBox(height: size.height * 0.03),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  )),
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: GridView.builder(
                    itemCount: MTray.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 20,
                      crossAxisSpacing: 50,
                      childAspectRatio: 1,
                    ),
                    itemBuilder: (context, index) => ItemCard(
                      Tray: MTray[index],
                      press: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) =>
                              Products(true, "Week Promotion", "123"),
                        ),
                      ),
                    ),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
