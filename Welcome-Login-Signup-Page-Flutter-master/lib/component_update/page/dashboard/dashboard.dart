import 'package:flutter/material.dart';
import 'package:flutter_auth/base/Common.dart';
import 'package:flutter_auth/component_update/page/components/backgroundDefault.dart';
import 'package:flutter_auth/component_update/page/components/buttonDashboard.dart';
import 'package:flutter_auth/component_update/page/detailpages/1.OTC/OTC.dart';
import 'package:flutter_auth/component_update/page/detailpages/2.ETC/function_ETC.dart';
import 'package:flutter_auth/component_update/page/detailpages/3.functional_food/function_FF.dart';
import 'package:flutter_auth/component_update/page/detailpages/4.personal_tool/function_PT.dart';
import 'package:flutter_auth/component_update/page/detailpages/5.bayby_care/function_BBC.dart';
import 'package:flutter_auth/component_update/page/detailpages/6.family_planning/function_FP.dart';
import 'package:intl/intl.dart';

class dashboard extends StatelessWidget {
  const dashboard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd ').format(now);
    var mediaQuery = MediaQuery.of(context);
    return Scaffold(
      body: Stack(children: <Widget>[
        BackgroundDefault(
          mediaQuery,
          // formattedDate,
          context,
        ),
        Positioned(
          top: 70,
          left: 30,
          child: Text(
            "Pharmarcy xin chào",
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
            ),
          ),
        ),
        Positioned(
          top: 100,
          left: 30,
          child: Text(
            "USERNAME",
            style: TextStyle(
              fontSize: 25,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Positioned(
          top: 140,
          left: 30,
          child: Text(
            formattedDate,
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      Positioned(
          top: 50,
          right: 10,
          child: IconButton(
            onPressed: () {} ,
            icon: Icon(
              Icons.notifications,
              color: Colors.white,
            ),
          ),
        ),
        Positioned(
          top: 210,
          left: 40,
          child: buttonDashboard(
            picture: "assets/images/6.jpg",
            text: 'ETC',
            press: (){changeScreen(context, function_ETC());},
          ),
        ),
        Positioned(
          top: 210,
          right: 40,
          child: buttonDashboard(
            picture: "assets/images/1.jpg",
            text: 'OTC',
            press: (){changeScreen(context, function_OTC());},
          ),
        ),
        Positioned(
          top: 385,
          left: 40,
          child: buttonDashboard(
            picture: "assets/images/2.jpg",
            text: 'Function Food',
            press: (){changeScreen(context, function_FF());},
          ),
        ),
        Positioned(
          top: 385,
          right: 40,
          child: buttonDashboard(
            picture: "assets/images/3.jpg",
            text: 'Personal Tool',
            press: (){changeScreen(context, function_PT());},
          ),
        ),
        Positioned(
          top: 560,
          left: 40,
          child: buttonDashboard(
            picture: "assets/images/4.jpg",
            text: 'Baby Care',
            press: (){changeScreen(context, function_BBC());},
          ),
        ),
        Positioned(
          top: 560,
          right: 40,
          child: buttonDashboard(
            picture: "assets/images/5.jpg",
            text: 'Family Planning',
            press: (){changeScreen(context, function_FP());},
          ),
        ),
      ]),
    );
  }
}
