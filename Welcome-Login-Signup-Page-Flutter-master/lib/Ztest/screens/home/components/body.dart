import 'package:flutter/material.dart';
import 'package:flutter_auth/Ztest/constants.dart';
import 'package:flutter_auth/Ztest/models/Product.dart';
import 'package:flutter_auth/Ztest/screens/details/details_screen.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';
import 'package:flutter_auth/component_update/page/components/testFieldDefault.dart';

import 'categorries.dart';
import 'item_card.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: kPrimaryLightColor,
      appBar: AppBar(title: Text("quầy thuốc"),elevation: 0,backgroundColor: kPrimaryLightColor,),
      body: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 65,
            child: TextFieldDefault(
              child: TextField(

                // controller: textControler,
                // onChanged: onChanged,
                cursorColor: kPrimaryColor,
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.person,
                    color: kPrimaryColor,
                  ),
                  hintText: "Nhập tên bệnh nhân",
                  // errorText: errorHint==null?"":errorHint,
                  // errorHint.hasError ? errorHint.error.toString() : null,
                  // labelStyle: TextStyle(
                  //     fontSize: 15, color: Colors.grey[300])),

                  border: InputBorder.none,
                ),
              ),
            ),

            // decoration: BoxDecoration(
            //     color: Theme.of(context).primaryColor,
            //   borderRadius: BorderRadius.only(
            //     bottomRight: Radius.circular(50),
            //     bottomLeft: Radius.circular(50)
            //   ))
            ),
          Container(
            height: 65,
            child: TextFieldDefault(
              child: TextField(

                // controller: textControler,
                // onChanged: onChanged,
                cursorColor: kPrimaryColor,
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.person,
                    color: kPrimaryColor,
                  ),
                  hintText: "Tuổi",
                  // errorText: errorHint==null?"":errorHint,
                  // errorHint.hasError ? errorHint.error.toString() : null,
                  // labelStyle: TextStyle(
                  //     fontSize: 15, color: Colors.grey[300])),

                  border: InputBorder.none,
                ),
              ),
            ),

            // decoration: BoxDecoration(
            //     color: Theme.of(context).primaryColor,
            //   borderRadius: BorderRadius.only(
            //     bottomRight: Radius.circular(50),
            //     bottomLeft: Radius.circular(50)
            //   ))
          ),
          Container(
            height: 65,
            child: TextFieldDefault(
              child: TextField(

                // controller: textControler,
                // onChanged: onChanged,
                cursorColor: kPrimaryColor,
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.person,
                    color: kPrimaryColor,
                  ),
                  hintText: "Chuẩn đoán",
                  // errorText: errorHint==null?"":errorHint,
                  // errorHint.hasError ? errorHint.error.toString() : null,
                  // labelStyle: TextStyle(
                  //     fontSize: 15, color: Colors.grey[300])),

                  border: InputBorder.none,
                ),
              ),
            ),

            // decoration: BoxDecoration(
            //     color: Theme.of(context).primaryColor,
            //   borderRadius: BorderRadius.only(
            //     bottomRight: Radius.circular(50),
            //     bottomLeft: Radius.circular(50)
            //   ))
          ),

          SizedBox(height: size.height * 0.03),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                )
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
                child: GridView.builder(
                    itemCount: products.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: kDefaultPaddin,
                      crossAxisSpacing: 50,
                      childAspectRatio: 1,
                    ),
                    itemBuilder: (context, index) => ItemCard(
                          product: products[index],
                          press: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => DetailsScreen(
                                  product: products[index],
                                ),
                              )),
                        )),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
