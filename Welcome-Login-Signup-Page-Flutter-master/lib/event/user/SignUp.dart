import 'package:flutter/cupertino.dart';
import 'package:flutter_auth/base/bloc_event.dart';

class SignUpEvent extends BaseEvent {
  String fullname;
  String email;
  String confirmPassword;
  String password;
  String phone;
  String address;
  BuildContext context;
  SignUpEvent(this.fullname, this.email, this.password, this.confirmPassword,
      this.phone, this.address,this.context);





}
