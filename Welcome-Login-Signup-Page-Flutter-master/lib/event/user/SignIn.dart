import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_auth/base/bloc_event.dart';

class SignInEvent extends BaseEvent {
  String userName;
  String passWord;
  BuildContext context;
  SignInEvent(this.userName, this.passWord, this.context);
}

class GetInfoEvent extends BaseEvent {
  String full_name;
  String token;
  String email;
  String address;
  String phone;
  GetInfoEvent(
    this.full_name,
    this.token,
    this.email,
    this.address,
    this.phone,
  );

  factory GetInfoEvent.fromJson1(dynamic json) {
    return GetInfoEvent(json['full_name'] as String, json['token'] as String, json['email'] as String, json['address'] as String, json['phone'] as String);
  }

  Map<String, dynamic> toJSON1() {
    return <String, dynamic>{
      'full_name': full_name,
      'token': token,
      'email': email,
      'address': address,
      'phone': phone,
    };
  }
  static Map<String, dynamic> toMap(GetInfoEvent get_info) => {
    'full_name': get_info.full_name,
    'token': get_info.token,
    'email': get_info.email,
    'address': get_info.address,
    'phone': get_info.phone,
  };
  factory GetInfoEvent.fromJson(Map<String, dynamic> jsonData) {
    return GetInfoEvent(
      jsonData['full_name'],
      jsonData['token'],
      jsonData['email'],
      jsonData['address'],
      jsonData['phone'],
    );
  }
  static String Encode1(GetInfoEvent get_info) {
    // String objText =
    //     '{"username": "${get_info.username}" , "token":"${get_info.token}" , "email": "${get_info.email},"address": "${get_info.address} , "phone": "${get_info.phone}"}';
    String objText = json.encode(GetInfoEvent.toMap(get_info));
    return objText;
  }
  static GetInfoEvent Decode1(String get_info) {
      GetInfoEvent info =  GetInfoEvent.fromJson1(json.decode(get_info));
    return info;
  }


}
