
import 'package:flutter_auth/base/bloc_event.dart';
class getToken extends BaseEvent{
  String token;

  getToken(this.token);
}
class ForgotPassEvent extends BaseEvent{
  String email;
  ForgotPassEvent(this.email);
}
