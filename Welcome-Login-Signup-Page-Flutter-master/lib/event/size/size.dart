import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_auth/base/bloc_event.dart';


class SizeEvent extends BaseEvent {
  String size;

  SizeEvent(
    this.size,
  );
  // Map<String, dynamic> toJson() => {
  //   'id': id,
  //   'size': size,
  //   'color': color,
  //   'quantity': quantity,
  //   'price': price,
  // };
  //  factory CartEvent.fromJson(Map<String, dynamic> parsedJson) {
  //   return new CartEvent('id', 'size', 'quantity', 'color', 'price');
  // }
  SizeEvent.fromData(_size) {
    size = _size;
  }
  factory SizeEvent.fromJson(Map<String, dynamic> jsonData) {
    return SizeEvent(
      jsonData['size'],
    );
  }
  Map<String, dynamic> toJSON1() {
    return <String, dynamic>{
      'size': size,
    };
  }

  static String encode1(List<SizeEvent> items) => json.encode(
        items.map<Map<String, dynamic>>((item) => item.toJSON1()).toList(),
      );

  static List<SizeEvent> decode1(String carts) =>
      (json.decode(carts) as List<dynamic>)
          .map<SizeEvent>((item) => SizeEvent.fromJson(item))
          .toList();
}
