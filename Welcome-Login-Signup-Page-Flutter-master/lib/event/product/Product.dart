// import 'package:flutter/cupertino.dart';
// import 'package:shop_flutter/base/bloc_event.dart';
//
// class ProductEvent extends BaseEvent {
//   String    name;
//   String description;
//   String gender;
//   String color;
//   String product_type;
//   List<String> product_size_list;
//   String price_origin;
//   BuildContext context;
//    ProductEvent.fromJson(Map<List<ProductEvent>, dynamic> json)
//       : name = json['name'],
//         description = json['description'],
//         gender = json['gender'],
//         color = json['color'],
//         product_type = json['product_type'],
//         product_size_list = json['product_size_list'],
//         price_origin = json['price_origin'];
//   Map<String, dynamic> toJson() => {
//     'name': name,
//     'description': description,
//     'gender': gender,
//     'color': color,
//     'product_type': product_type,
//     'product_size_list': product_size_list,
//     'price_origin': price_origin
//   };
//   ProductEvent(this.name, this.description, this.gender, this.color,
//       this.product_type, this.product_size_list, this.price_origin, context);
// }
import 'package:flutter/material.dart';
import 'package:flutter_auth/base/bloc_event.dart';

class ProductEvent extends BaseEvent {
  String id;
  String name;
  String description;
  String gender;
  String img;
  String color;
  String product_type;
  String rating;
  String quantity_sold;
  List<String> product_size_list;
  String price_origin;
  BuildContext context;
  ProductEvent(
      {this.id,
      this.name,
      this.description,
      this.gender,
      this.color,
      this.product_type,
      this.img,
      this.rating,
      this.quantity_sold,
      this.product_size_list,
      this.price_origin,
      this.context});
  factory ProductEvent.fromJson(Map<String, dynamic> json) {
    return ProductEvent(
      id: json['_id'],
      name: json['name'],
      description: json['description'],
      gender: json['gender'],
      color: json['color'],
      product_type: json['product_type'],
      img: json['img'],
      rating: json['rating'].toString(),
      quantity_sold: json['quantity_sold'].toString(),
      product_size_list: json['product_size_list'],
      price_origin: json['price_origin'],
    );
  }
}
class SearchEvent extends BaseEvent{
  String key ;
  SearchEvent(this.key);
}
class LoadProductEvent extends BaseEvent {
  List<ProductEvent> listOfProducts;
  LoadProductEvent(this.listOfProducts);
}
