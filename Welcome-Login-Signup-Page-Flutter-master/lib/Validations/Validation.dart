class Validations{
  static bool isValidUser(String user){
    return user!= null && user.length>6 ;//&& user.contains("@");
  }
  static bool isValidPass( String pass){
    return pass != null && pass.length>0;
  }
  static bool isValidFirstName( String first_name){
    return first_name != null && first_name.length>0;
  }
  static bool isValidFullName( String full_name){
    return full_name != null && full_name.length>0;
  }
  static bool isValidLastName( String last_name){
    return last_name != null && last_name.length>0;
  }
  static bool isConfirmPass( String pass, String confirmPass){
    return pass != confirmPass;

  }
  static bool isValidEmail( String email){
    return email != null && email.length>0;
  }
  static bool isValidPhone( String phone){
    return phone != null && phone.length>0;
  }
  static bool isValidSex( String sex){
    return sex != null && sex.length>0;
  }
  static bool isValidAddress( String address){
    return address != null && address.length>0;
  }
  static bool isValiddOb( String dOb){
    return dOb != null && dOb.length>0;
  }
}