// const mess_user_invalid = "Tài khoản không hợp lệ";
// const mess_short_user_invalid = "Tài khoản quá ngắn";
// const mess_pass_invalid="mật khẩu không hợp lệ";
// const mess_short_pass_invalid="mật khẩu quá ngắn";
final RegExp emailValidatorRegExp =
RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNamelNullError = "Please Enter your name";
const String kPhoneNumberNullError = "Please Enter your phone number";
const String kAddressNullError = "Please Enter your address";

const String mess_NullError = "Please Enter your ";
const String mess_InvalidEmailError = "Please Enter Valid ";
const String mess_ShortError = "is too short";



