//import 'package:flutter_session/flutter_session.dart';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_auth/API/global.dart';
import 'package:flutter_auth/event/product/Product.dart';
import 'package:flutter_auth/event/size/size.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Product_Service {
  dynamic change_home(BuildContext context) {
    Navigator.pushNamed(context, '/home');
  }

  static Future<List<ProductEvent>> fetchProduct() async {
    // try {
    print('fetchProduct');

    final response = await http.post(Uri.parse('$ProductList?list_type=0'));

    if (response.statusCode == 200) {
      Map<String, dynamic> mapResponse = json.decode(response.body);

      //Load view
      final products = mapResponse["data"].cast<Map<String, dynamic>>();
      print(products);
      final listOfProducts = await products.map<ProductEvent>((json) {
        return ProductEvent.fromJson(json);
      }).toList();
      return listOfProducts;
    } else {
      throw Exception("Failed");
    }
    // } finally {
    //   // done you can do something here
    // }
  }

  static Future<List<ProductEvent>> fetchProduct_best_sell() async {
    // try {
    print('fetchProduct1  ');
    final response = await http.post(Uri.parse('$ProductList?list_type=1'));

    if (response.statusCode == 200) {
      Map<String, dynamic> mapResponse = json.decode(response.body);

      //Load view
      final products = mapResponse["data"].cast<Map<String, dynamic>>();

      final listOfProducts = await products.map<ProductEvent>((json) {
        return ProductEvent.fromJson(json);
      }).toList();
      return listOfProducts;
    } else {
      throw Exception("Failed");
    }
    // } finally {
    //   // done you can do something here
    // }
  }

  static Future<List<ProductEvent>> Search(String key_name, String key_type,
      List<ProductEvent> _productListData) async {
    print(key_type);
    List<ProductEvent> _search = _productListData;
    if (key_type != "" && key_type != null) {
      _search = _productListData.where((element) {
        var note = element.product_type.toLowerCase();
        return note.contains(key_type.toLowerCase());
      }).toList();
    }
    if (key_name != "" && key_name != null) {
      _search = _search.where((element) {
        var note = element.name.toLowerCase();
        return note.contains(key_name);
      }).toList();
    }

    return _search;
    // } finally {
    //   // done you can do something here
    // }
  }

  static Future<List<ProductEvent>> fetchProductbyType(
      String product_type_id) async {
    // try {
    print('fetchProductByType');
    print('$GetProductByType?product_type_id=${product_type_id}');
    final response = await http.post(
        Uri.parse('$GetProductByType?product_type_id=${product_type_id}'));
    if (response.statusCode == 200) {
      Map<String, dynamic> mapResponse = json.decode(response.body);
      //Load view
      final products = mapResponse["Products"].cast<Map<String, dynamic>>();
      final listOfProducts = await products.map<ProductEvent>((json) {
        return ProductEvent.fromJson(json);
      }).toList();

      return listOfProducts;
    } else {
      throw Exception("Failed");
    }
    // } finally {
    //   // done you can do something here
    // }
  }

  static Future<List<SizeEvent>> GetSizeInProduct(String product_id) async {
    // try {
    print('GetSizeInProduct');
    // print(product_id);
    final response =
    await http.post(Uri.parse('$FindProductt?_id=${product_id}'));
    if (response.statusCode == 200) {
      Map<String, dynamic> mapResponse = json.decode(response.body);
      //Load view
      final sizes =
      mapResponse["data"]["product_size_list"].cast<Map<String, dynamic>>();

      final listOfSizes = await sizes.map<SizeEvent>((json) {
        return SizeEvent.fromJson(json);
      }).toList();

      return listOfSizes;
    } else {
      throw Exception("Failed");
    }
    // } finally {
    //   // done you can do something here
    // }
  }
}
