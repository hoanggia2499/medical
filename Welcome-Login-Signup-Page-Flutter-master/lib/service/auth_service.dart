import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_auth/API/global.dart';
import 'package:flutter_auth/event/user/Auth.dart';
import 'package:flutter_auth/event/user/SignIn.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AuthService {
  final baseUrl = 'http://localhost:4000';
  // ignore: non_constant_identifier_names
  //static final SESSION = FlutterSession();

  Future<dynamic> register(String full_name, String email, String password,
      String phone, String address) async {
    try {
      print('goi api register');
      var res = await http.post(Uri.parse(
          '$Register?fullname=$full_name&password=$password&email=$email&phone=$phone&address=$address'));
      return res;
    } finally {
      // done you can do something here
    }
  }

  Future<dynamic> forgetPass(String email) async {
    try {
      print('goi api forgetPass');
      var res = await http.post(Uri.parse('$ForgetPass$email'));

      if (res.statusCode == 200) {
        var jsonData = json.decode(res.body);
        if (jsonData['message'][0] == "Successfully") {
          return jsonData;
        }
        return false;
      }
      return false;
    } finally {
      // done you can do something here
    }
  }

  Future<dynamic> resetPass(String user_id, String code) async {
    try {
      print('goi api resetPass');
      print(code);
      var res =
      await http.post(Uri.parse('$ResetPass?user_id=$user_id&code=$code'));
      print(res.body);
      var jsonData = json.decode(res.body);
      print(jsonData['data']);
      return jsonData['data'];
    } finally {
      // done you can do something here
    }
  }

  Future<dynamic> UpdateInfo(String full_name, String address) async {
    print('http');
    print('$UpdateInfo1?address=$address&code=$full_name');
    String token = await getToken();
    var res = await http.post(Uri.parse(
      '$UpdateInfo1&address=$address&fullname=$full_name',),headers: ({
      'Authorization': 'Bearer $token',
    }));
    String a = json.encode(res.body);
    print(a);
  }

  static Future<dynamic> ChangePass(
      String new_password, String old_password) async {
    // print(new_password);
    // return new_password;
    String token = await getToken();
    var res = await http.post(
        Uri.parse(
            '$ChangePasss&new_password=$new_password&old_password=$old_password'),
        headers: {
          'Authorization': 'Bearer  $token',
        });

  }

  Future<dynamic> login(String userName, String passWord) async {
    try {
      print('goi api login');
      var res = await http
          .post(Uri.parse("$Login?user=$userName&password=$passWord"));
      return res;
    } finally {
      // you can do somethig here
    }
  }

  static String getUser() {
    LocalStorage get_info = new LocalStorage('info');
    String info = get_info.getItem('info');
    return info;
    // var res =  http
    //     .post(Uri.parse("$get_token$token"));
  }

  // static Future<String> test() async {
  //    final  storage = new LocalStorage('carts');
  //   final listItem = await  storage.getItem('carts');
  //   // Future<dynamic> a = storage.getItem('carts');
  // return listItem;
  // }
  // static Future<int> test2() async {
  //   print('total');
  //   final  storage = new LocalStorage('carts');
  //   final List<CartEvent> list_cart = await  CartEvent.decode1(storage.getItem('carts'));
  //   var totalPrice= 0;
  //   print(list_cart[0].price);
  //   list_cart.map((e) {
  //     totalPrice+= int.parse(e.price)  * int.parse(e.quantity);
  //   }).toString();
  //   // Future<dynamic> a = storage.getItem('carts');
  //
  //   return totalPrice;
  // }
  void setToken(GetInfoEvent info) async {
    print('setToken');
    String info_to_string = GetInfoEvent.Encode1(info);
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('token', info.token);
    prefs.setString('info', info_to_string);
  }

  static Future<String> getToken() async {
    print('getToken');
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    return token;
    // return await SESSION.get('tokens');
  }

  static Future<GetInfoEvent> getInfo() async {
    print('getInfo');
    final prefs = await SharedPreferences.getInstance();
    String get_info = prefs.getString('info');
    GetInfoEvent info = GetInfoEvent.Decode1(get_info);
    // print(a);
    return info;
  }

  void removeToken() async {
    print('RemoveToken');
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    await http.post(Uri.parse("$LogOut$token"),
        headers: {'Authorization': 'bearer ${token}'});
    prefs.clear();
    print(prefs.getString('token'));
  }
}
