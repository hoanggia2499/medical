import 'package:flutter/material.dart';
import 'package:flutter_auth/bloc/login_bloc/auth_bloc.dart';


class Provider extends InheritedWidget {
  final bloc = new AuthBloc();
  // constructor, forward to Parent InheritedWidget
  Provider({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget _) {
    // Todo implantation
    return true;
  }

  static AuthBloc of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Provider>().bloc;
  }
  // static SignUpBloc a(BuildContext context) {
  //   return context.dependOnInheritedWidgetOfExactType<Provider>().signUp_bloc;
  // }
}