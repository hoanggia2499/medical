//
// import 'package:flutter/material.dart';
// import 'package:flutter_auth/component_update/constant/constant.dart';
// import 'components/emptyAppBar.dart';
// class Intro extends StatefulWidget {
//   String introImage, descText, headText;
//   Intro({Key key, this.introImage, this.descText, this.headText});
//   @override
//   _IntroState createState() => _IntroState();
// }
//
// class _IntroState extends State<Intro> with SingleTickerProviderStateMixin {
//   AnimationController _controller;
//
//   @override
//   void initState() {
//     super.initState();
//     _controller = AnimationController(vsync: this);
//   }
//
//   @override
//   void dispose() {
//     _controller.dispose();
//     super.dispose();
//   }
//
//   var _controler = PageController();
//   var _currentPage = 0;
//   @override
//   Widget build(BuildContext context) {
//     _controler.addListener(() {
//       setState(() {
//         _currentPage = _controler.page.round();
//       });
//     });
//     return Scaffold(
//         backgroundColor: kAccentColor,
//         appBar: EmptyAppBar(),
//         body: Column(
//           children: [
//             Expanded(
//               //SAO CAI THOAI NO BI LAG Z ANH BAO; CHO EM VO MENU CUA NO VOI
//               child: PageView.builder(
//                 itemCount: introData.length,
//                 itemBuilder: (context, index) => IntroData(
//                   introImage: introData[index]['image'],
//                   headText: introData[index]['headText'],
//                   descText: introData[index]['descText'],
//                 ),
//                 onPageChanged: (page) {
//                   setState(() {
//                     _currentPage = page;
//                   });
//                 },
//               ),
//             ),
//             Botton(currentPage: _currentPage)
//           ],
//         ));
//   }
// }
//
