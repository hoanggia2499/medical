
import 'package:flutter/material.dart';
import 'package:flutter_auth/component_update/constant/constant.dart';

class EmptyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final value;
  const EmptyAppBar({

    Key key,
    this.value
  }) : super(key: key);
  @override
  Size get preferredSize => const Size.fromHeight(kAppBarHeight);
  Widget build(BuildContext context) {
    return AppBar(
      title: value,
      backgroundColor: kWhiteColor,
      elevation: kRadius ,
      automaticallyImplyLeading: false,
    );
  }
}