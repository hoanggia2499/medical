import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth/Validations/Validation.dart';
import 'package:flutter_auth/base/Common.dart';
import 'package:flutter_auth/base/base_bloc.dart';
import 'package:flutter_auth/base/bloc_event.dart';
import 'package:flutter_auth/component_update/page/components/bottomNavBar.dart';
import 'package:flutter_auth/event/user/Auth.dart';
import 'package:flutter_auth/event/user/SignIn.dart';
import 'package:flutter_auth/service/auth_service.dart';

class AuthBloc extends BaseBloc {
  StreamController _userNameController = new StreamController();
  StreamController _passWordController = new StreamController();

  Stream get userNameStream => _userNameController.stream;
  Stream get passWordStream => _passWordController.stream;

  var authInfo ;
  var user ;
  var password;

  bool isValidInfo(String username, String pass) {
    if (!Validations.isValidUser(username)) {
      _userNameController.sink.addError("Tài khoản không hợp lệ");
      return false;
    }
    _userNameController.sink.add("ok");

    if (!Validations.isValidPass(pass)) {
      _passWordController.sink.addError("Mật khẩu không hợp lệ");
      return false;
    }
    _passWordController.sink.add("ok");

    return true;
  }

  // void _signIn(String username, String pass) async{
  //
  //   if(isValidInfo(username, pass))
  //   {
  //     authInfo = AuthService();
  //
  //     final res = await authInfo.login(username, pass);
  //     var jsonData= null ;
  //     var response= await http.post(Uri.parse("$Login?user=$username&password=$pass"));
  //     if(response.statusCode==200){
  //       jsonData= json.decode(response.body);
  //       if(jsonData['message'][0]=="Successfully")
  //       {
  //         var token = jsonData['data']['token'];
  //         final prefs = await SharedPreferences.getInstance();
  //         prefs.setString('token', token);
  //         print( prefs.getString('token'));
  //
  //         //Navigator.of(context).pushAndRemoveUntil(newRoute, (route) => false)
  //       }
  //       else{
  //         print("dang nhap that bai");
  //       }
  //     }
  //   }
  // }
  void _signIn(String username, String pass, BuildContext context) async {
    if (isValidInfo(username, pass)) {
      authInfo = AuthService();
      final res = await authInfo.login(username, pass);
      var jsonData = null;
      print(res.statusCode);
      if (res.statusCode == 200) {
        jsonData = json.decode(res.body);
        if (jsonData['message'][0] == "Successfully") {
          GetInfoEvent getInfo = new GetInfoEvent.fromJson(jsonData['data']);
          print(getInfo.full_name);
          authInfo.setToken(getInfo);
         // Navigator.push(context, MaterialPageRoute(builder: gotohome()));
         //   changeScreen(context,BottomNavBar(0));
        } else {
          print("dang nhap that bai");
        }
      }
    }
  }
  Future<bool> _forgotPass(String email) async {
    var callAuthService = AuthService();
    var res = await callAuthService.forgetPass(email);
    if(res.statusCode==200){
      var jsonData = json.decode(res.body);
      if(jsonData['message'][0]=="Successfully"){
        return true;
      }
      return false;
    }
    return false;

  }
  @override
  void dispatchEvent(BaseEvent event) {
    if (event is SignInEvent) {
      _signIn(event.userName, event.passWord, event.context);
    }
    if(event is ForgotPassEvent){
      _forgotPass(event.email);
    }


  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _userNameController.close();
    _passWordController.close();
  }
}
