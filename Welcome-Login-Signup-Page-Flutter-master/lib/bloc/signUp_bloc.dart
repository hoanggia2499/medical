import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth/Validations/Validation.dart';
import 'package:flutter_auth/base/Common.dart';
import 'package:flutter_auth/base/base_bloc.dart';
import 'package:flutter_auth/base/bloc_event.dart';
import 'package:flutter_auth/component_update/sign_up_in/Screens/Login/login_screen.dart';
import 'package:flutter_auth/event/user/SignUp.dart';
import 'package:flutter_auth/service/auth_service.dart';

class SignUpBloc extends BaseBloc {
  StreamController _full_nameController = new StreamController();
  StreamController _confirmPasswordController = new StreamController();
  StreamController _passwordController = new StreamController();
  StreamController _emailController = new StreamController();
  StreamController _phoneController = new StreamController();
  StreamController _addressController = new StreamController();


  Stream get full_nameStream => _full_nameController.stream;

  Stream get passwordStream => _passwordController.stream;
  Stream get confirmPassword => _confirmPasswordController.stream;
  Stream get phoneStream => _phoneController.stream;
  Stream get emailStream => _emailController.stream;
  Stream get addressStream => _addressController.stream;


  var authInfo;
  bool isValidInfo(String full_name, String confirmPassword, String email,
      String password, String phone, String address) {
    print('kiem tra');
    var flag=0;
    if (!Validations.isValidFullName(full_name)) {
      _full_nameController.sink.addError("Tên không hợp lệ");
        flag=1;
    }
    if (!Validations.isValidPass(password)) {
      _passwordController.sink.addError("Mật khẩu không hợp lệ");
      flag=1;
    }
    if (!Validations.isConfirmPass(password,confirmPassword)) {
      _confirmPasswordController.sink.addError("Tên không hợp lệ");
      flag=1;
    }
    if (!Validations.isValidEmail(email)) {
      _emailController.sink.addError("Email không hợp lệ");
      flag=1;
    }

    if (!Validations.isValidPhone(phone)) {
      _phoneController.sink.addError("Số điện thoại không hợp lệ");
        flag=1;
    }
    if (!Validations.isValidAddress(address)) {
      _addressController.sink.addError("Địa chỉ không hợp lệ");
      flag=1;
    }


    if(flag==1){
      return false;
    }
    _full_nameController.sink.add("ok");
    _confirmPasswordController.sink.add("ok");
    _emailController.sink.add("ok");
    _passwordController.sink.add("ok");
    _phoneController.sink.add("ok");
   _addressController.sink.add("ok");
    return true;
  }

  void _signUp(
      String full_name,
      String email,
      String password,
      String confirmPassword,

      String phone,
      String address,
      BuildContext context) async {
    if (isValidInfo(full_name, confirmPassword, email, password, phone, address)) {
      authInfo = AuthService();
      final res = await authInfo.register(
          full_name, email, password, phone, address);
      var jsonData = null;
      if (res.statusCode == 200) {
        jsonData = json.decode(res.body);
        if (jsonData['message'][0] == "Successfully") {
          print(jsonData['data']);
          changeScreen(context, LoginScreen());
        } else {

          //_messController.sink.add( jsonData['message'][0]);
        }
      }
      if(res.statusCode == 400){
        //_messController.sink.add( jsonData['message'][0]);
      }
    }
  }

  @override
  void dispatchEvent(BaseEvent event) {
    if (event is SignUpEvent) {
      print("vo event");
      _signUp(event.fullname,event.email,event.password,event.confirmPassword,event.phone,event.address, event.context);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _full_nameController.close();
    _confirmPasswordController.close();
    _emailController.close();
    _passwordController.close();
    _phoneController.close();
    _addressController.close();
    //_messController.close();
  }
}
